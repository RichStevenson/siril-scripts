### Description
This merge request addresses (... and describe the problem being addressed or new script purpose).

### Changes Made
Provide code snippets or screenshots as needed.

### Related Issues
Provide links to the related issues or feature requests.

### Additional Notes
Include any extra information or considerations for reviewers such as what platforms the code
has been tested on, what external dependencies are needed (e.g. for scripts that interface to other
software) etc.

### Merge Request Checklists
- [ ] The script complies with the [Commands Reference](https://siril.readthedocs.io/en/latest/Commands.html)
      (.SSF scripts) or [sirilpy API](https://siril.readthedocs.io/en/latest/Python-API.html) (python scripts).
- [ ] I have avoided unnecessary or excessive imports (python scripts).
- [ ] I have included a version and licence in my script (a SPDX licence identifier is sufficient).
- [ ] I have included author details and a means of contact (may be social media) for reporting issues.
- [ ] I acknowledge I am responsible for maintaining my script and that unmaintained scripts may be
      removed if they stop working.

